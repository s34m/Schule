(TeX-add-style-hook
 "arbeitsauftrag_u4"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt" "french")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true") ("inputenc" "utf8") ("fontenc" "T1")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "standalone"
    "inputenc"
    "babel"
    "fontenc"
    "fancyhdr"))
 :latex)

