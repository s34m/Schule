(TeX-add-style-hook
 "rede_s_25_2"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "standalone"
    "fancyhdr"))
 :latex)

