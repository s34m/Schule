(TeX-add-style-hook
 "biologie"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "tex/Immunsystem"
    "tex/allergien"
    "book"
    "bk10"
    "standalone"
    "fancyhdr"))
 :latex)

