(TeX-add-style-hook
 "franzoesisch"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "tex/u3"
    "tex/u4"
    "tex/u5"
    "book"
    "bk12"
    "fancyhdr"))
 :latex)

