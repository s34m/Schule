(TeX-add-style-hook
 "religion"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "Die Schöpfung"
    "book"
    "bk12"
    "standalone"
    "fancyhdr"))
 :latex)

