(TeX-add-style-hook
 "mathematik"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "tex/wiederholung19_20"
    "tex/kreis"
    "book"
    "bk10"
    "amsmath"
    "amssymb"
    "amsfonts"
    "pgfplots"
    "tikz"
    "standalone"
    "fancyhdr"))
 :latex)

