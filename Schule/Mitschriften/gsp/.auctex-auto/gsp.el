(TeX-add-style-hook
 "gsp"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "tex/habsburgermonarchie"
    "tex/amerika"
    "book"
    "bk10"
    "standalone"
    "fancyhdr"))
 :latex)

