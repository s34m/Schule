(TeX-add-style-hook
 "laboranalysePraesentation"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"))
 :latex)

