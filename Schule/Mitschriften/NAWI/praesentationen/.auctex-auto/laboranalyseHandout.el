(TeX-add-style-hook
 "laboranalyseHandout"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "standalone"
    "fancyhdr"))
 :latex)

