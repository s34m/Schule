(TeX-add-style-hook
 "laboranalyseFertig"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "a4paper" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true") ("hyperref" "breaklinks") ("inputenc" "utf8")))
   (TeX-run-style-hooks
    "latex2e"
    "tex/laboranalyse"
    "book"
    "bk10"
    "standalone"
    "hyperref"
    "inputenc"
    "fancyhdr"))
 :latex)

