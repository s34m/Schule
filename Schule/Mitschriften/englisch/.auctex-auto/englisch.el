(TeX-add-style-hook
 "englisch"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "10pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "tex/space"
    "tex/book_report"
    "tex/current"
    "tex/coffee"
    "book"
    "bk10"
    "standalone"
    "fancyhdr"))
 :latex)

