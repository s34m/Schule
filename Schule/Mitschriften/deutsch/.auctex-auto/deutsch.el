(TeX-add-style-hook
 "deutsch"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("book" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("standalone" "subpreambles=true")))
   (TeX-run-style-hooks
    "latex2e"
    "tex/rhetorik"
    "book"
    "bk12"
    "standalone"
    "fancyhdr"))
 :latex)

